package com.mankhong.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.service.controls.ControlsProviderService.TAG
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.remote.FirestoreChannel
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.tasks.await
import java.util.jar.Attributes.Name

class MainActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    val db = FirebaseFirestore.getInstance()


    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        auth = Firebase.auth
        setContentView(R.layout.activity_main)
        findViewById<Button>(R.id.button3).setOnClickListener {
            val mail = findViewById<EditText>(R.id.editMail).text.toString()
            val pass = findViewById<EditText>(R.id.editPass).text.toString()
            signin(mail,pass)

        }
        findViewById<Button>(R.id.rebutton).setOnClickListener {
            val mail = findViewById<EditText>(R.id.editMail).text.toString()
            val pass = findViewById<EditText>(R.id.editPass).text.toString()
            lifecycleScope.launch {
                signUp(mail,pass)
            }








        }

    }
    public override fun onStart() {

        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser
        if(currentUser != null){
            reload();
        }
    }
    private fun signin (email: String , password: String){
        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("status", "signInWithEmail:success")
                    val user = auth.currentUser
                    updateUI(user)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("status", "signInWithEmail:failure", task.exception)
                    Toast.makeText(baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT).show()
                    updateUI(null)
                }
            }

    }
    private fun updateUI(user: FirebaseUser?) {
        if (user != null) {
           setContentView(R.layout.sign_in)
        }
    }
    private suspend fun  signUp(email: String, password: String){
        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "createUserWithEmail:success")
                    val user = auth.currentUser

                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(TAG, "createUserWithEmail:failure", task.exception)
                    Toast.makeText(baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT).show()

                }
            }.await()
        val user :MutableMap<String,Any> = HashMap()
        user["Email"] = email
        user["Name"] = "Alanb"
        user["Password"] = password
        Toast.makeText(baseContext, email,
            Toast.LENGTH_SHORT).show()

        db.collection("Users").document(auth.currentUser?.uid.toString())
            .set(user)
            .addOnSuccessListener { documentReference ->
                Log.d(TAG, "DocumentSnapshot added with ID: "+ documentReference.toString())
            }
            .addOnFailureListener { e ->
                Log.w(TAG, "Error adding document", e)
            }
    }
    private fun reload() {}
}